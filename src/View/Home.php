<?php

namespace src\View;

use src\AbstractView;

class Home extends AbstractView {


    public function __construct() {
        $this->layout = "index.html.twig";
    }

}