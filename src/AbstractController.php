<?php

namespace src;

    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Tools\Setup;
    use Slim\Http\Request;
    use Slim\Slim;

    abstract class AbstractController {
	
    public $request; // HttpRequest

    function __construct(Request $request) {
        $this->request = $request;
        $this->app = Slim::getInstance();
        $this->em = $this->createEntityManager();
    }

    private function createEntityManager(){
        $path = array('src/Entity');
        $devMode = true;
        $config = Setup::createAnnotationMetadataConfiguration($path, $devMode);
        $connectionOptions = parse_ini_file("config/config.ini");
        return EntityManager::create($connectionOptions, $config);
    }
}