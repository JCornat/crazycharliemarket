<?php


namespace src\Controller;

use src\AbstractController;
use src\Entity\User;

class AuthenticationController extends AbstractController {


    public function checkSession() {
        $data = array();
        $data['status'] = 400;
        if(array_key_exists('id', $_SESSION)) {
            $data['status'] = 200;
        }
        echo json_encode($data);
    }

    public function checkAdminRights() {
        $data = array();
        $data['status'] = 400;
        if(array_key_exists('id', $_SESSION) && array_key_exists('admin', $_SESSION)) {
            $data['status'] = 200;
        }
        echo json_encode($data);
    }

    public function destroySession() {
        session_destroy();
        session_commit();
        session_start();
    }

}