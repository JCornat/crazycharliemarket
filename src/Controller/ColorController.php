<?php


namespace src\Controller;


use src\AbstractController;
use src\Entity\Color;

class ColorController extends AbstractController {

    //TODO Check rights
    public function get($id = null) {
        $data = array();
        $data['status'] = 200;

        try {
            if ($id === null) {
                $objects = $this->em->getRepository('src\Entity\Color')->findAll();
                foreach ($objects as $object) {
                    $data['colors'][] = $this->colorToArray($object);
                }
            } else {
                $object = $this->em->getRepository('src\Entity\Color')->find(array('id' => $id));
                if($object == null) {
                    throw new \Exception('Color not found');
                }
                $data['colors'] = $this->colorToArray($object);
            }
        } catch (\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function add() {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $color = new Color();
            $color->setName($param->name);
            $color->setHex($param->hex);

            $this->em->persist($color);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function edit($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $color = $this->em->find('src\Entity\Color', $id);
            if($color == null) {
                throw new \Exception('Item not found');
            }
            $color->setName($param->name);
            $color->setHex($param->hex);

            $this->em->persist($color);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function delete($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();

        try {
            $color = $this->em->find('src\Entity\Color', $id);
            if($color == null) {
                throw new \Exception('Color not found');
            }

            $this->em->remove($color);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    private function colorToArray(Color $object) {
        return array(
            'id' => $object->getId(),
            'name' => $object->getName(),
            'hex' => $object->getHex(),
            'createdAt' => $object->getCreatedAt()->format('d-m H:i'),
            'udpatedAt' => $object->getCreatedAt()->format('d-m H:i'),
        );
    }
}