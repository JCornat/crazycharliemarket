<?php


namespace src\Controller;


use src\AbstractController;
use src\Entity\Piece;
use src\Entity\Type;

class PieceController extends AbstractController {

    //TODO Check rights
    public function get($id = null) {
        $data = array();
        $data['status'] = 200;

        try {
            if ($id === null) {
                $pieces = $this->em->getRepository('src\Entity\Piece')->findAll();
                foreach ($pieces as $piece) {
                    $data['pieces'][] = $this->pieceToArray($piece);
                }
            } else {
                $type = $this->em->getRepository('src\Entity\Piece')->find(array('id' => $id));
                if($type == null) {
                    throw new \Exception('Piece not found');
                }
                $data['pieces'] = $this->pieceToArray($type);
            }
        } catch (\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function add() {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $piece = new Piece();
            $piece->setName($param->name);
            $piece->setDescription($param->description);

            $this->em->persist($piece);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function edit($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $piece = $this->em->find('src\Entity\Piece', $id);
            if($piece == null) {
                throw new \Exception('Piece not found');
            }
            $piece->setName($param->name);
            $piece->setDescription($param->description);

            $this->em->persist($piece);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function delete($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();

        try {
            $piece = $this->em->find('src\Entity\Piece', $id);
            if($piece == null) {
                throw new \Exception('Piece not found');
            }

            $this->em->remove($piece);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    private function pieceToArray(Piece $object) {
        return array(
            'id' => $object->getId(),
            'name' => $object->getName(),
            'description' => $object->getDescription(),
            'createdAt' => $object->getCreatedAt()->format('d-m H:i'),
            'udpatedAt' => $object->getCreatedAt()->format('d-m H:i'),
        );
    }

}