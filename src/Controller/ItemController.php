<?php


namespace src\Controller;


use src\AbstractController;
use src\Entity\Item;

class ItemController extends AbstractController {

    //TODO Check rights
    public function get($id = null) {
        $data = array();
        $data['status'] = 200;

        try {
            if ($id === null) {
                $items = $this->em->getRepository('src\Entity\Item')->findAll();
                foreach ($items as $item) {
                    $data['items'][] = $this->itemToArray($item);
                }
            } else {
                $item = $this->em->getRepository('src\Entity\Item')->find(array('id' => $id));
                if($item == null) {
                    throw new \Exception('Item not found');
                }
                $data['items'] = $this->itemToArray($item);
            }
        } catch (\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    //TODO Check rights
    public function add() {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $errors = array();
            $item = new Item();
            $item->setName($param->name);
            $item->setDescription($param->description);

            $color = $this->em->getRepository('src\Entity\Color')->find($param->colorId);
            if($color == null) {
                throw new \Exception('Color not found');
            }
            $item->setColor($color);

            $item->setImage($param->image);
            $type = $this->em->getRepository('src\Entity\Type')->find($param->typeId);
            if($type == null) {
                throw new \Exception('Type not found');
            }
            $item->setType($type);

            $item->setPrice($param->price);

            $piece = $this->em->getRepository('src\Entity\Piece')->find($param->pieceId);
            if($piece == null) {
                throw new \Exception('Piece not found');
            }
            $item->setPiece($piece);

            $item->setPopularity($param->popularity);

            $this->em->persist($item);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    //TODO Check rights
    public function edit($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $errors = array();
            $item = $this->em->find('src\Entity\Item', $id);
            if($item == null) {
                throw new \Exception('Item not found');
            }
            $item->setName($param->name);
            $item->setDescription($param->description);

            $color = $this->em->getRepository('src\Entity\Color')->find($param->colorId);
            if($color == null) {
                throw new \Exception('Color not found');
            }
            $item->setColor($color);

            $item->setImage($param->image);
            $type = $this->em->getRepository('src\Entity\Type')->find($param->typeId);
            if($type == null) {
                throw new \Exception('Type not found');
            }
            $item->setType($type);

            $item->setPrice($param->price);

            $piece = $this->em->getRepository('src\Entity\Piece')->find($param->pieceId);
            if($piece == null) {
                throw new \Exception('Piece not found');
            }
            $item->setPiece($piece);

            $item->setPopularity($param->popularity);

            $this->em->persist($item);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function checked($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $item = $this->em->find('src\Entity\Item', $id);
            if($item == null) {
                throw new \Exception('Item not found');
            }
            $item->setSelected($param);

            $this->em->persist($item);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    //TODO Check rights
    public function plus($id) {
        $data = array();
        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $errors = array();
            $item = $this->em->find('src\Entity\Item', $id);
            if($item == null) {
                throw new \Exception('Item not found');
            }
            $popularity = $item->getPopularity();
            $popularity++;
            $item->setPopularity($popularity);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($item);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function delete($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();

        try {
            $item = $this->em->find('src\Entity\Item', $id);
            if($item == null) {
                throw new \Exception('Item not found');
            }

            $this->em->remove($item);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    private function itemToArray(Item $item) {
        $a =  array(
            'id' => $item->getId(),
            'name' => $item->getName(),
            'description' => $item->getDescription(),
            'pieceId' => $item->getPiece()->getId(),
            'piece' => $item->getPiece()->getName(),
            'image' => $item->getImage(),
            'typeId' => $item->getType()->getId(),
            'type' => $item->getType()->getName(),
            'popularity' => $item->getPopularity(),
            'selectedItem' => $item->getSelected(),
            'price' => $item->getPrice(),
            'createdAt' => $item->getCreatedAt()->format('d-m H:i'),
            'updatedAt' => $item->getUpdatedAt()->format('d-m H:i'),
        );

        if($item->getColor() !== null) {
            $a['colorId'] = $item->getColor()->getId();
            $a['color'] = $item->getColor()->getName();
        }

        return $a;
    }

    public function sanitizeName($name, &$address) {
        $address->setName(htmlspecialchars(filter_var($name), FILTER_SANITIZE_STRING));
    }

    public function sanitizeCity($city, &$address) {
        $address->setCity(htmlspecialchars(filter_var($city), FILTER_SANITIZE_STRING));
    }

    public function sanitizeStreetAddress1($streetAddress, &$address) {
        $address->setStreetAddress1(htmlspecialchars(filter_var($streetAddress), FILTER_SANITIZE_STRING));
    }

    public function sanitizeStreetAddress2($streetAddress, &$address) {
        $address->setStreetAddress2(htmlspecialchars(filter_var($streetAddress), FILTER_SANITIZE_STRING));
    }

    public function sanitizePostalCode($postalCode, &$address) {
        $address->setPostalCode(htmlspecialchars(filter_var($postalCode), FILTER_SANITIZE_STRING));
    }

    public function sanitizePhoneNumber($phoneNumber, &$address) {
        $address->setPhoneNumber(htmlspecialchars(filter_var($phoneNumber), FILTER_SANITIZE_NUMBER_INT));
    }

    public function sanitize($data, &$address, &$errors) {
        if (isset($data->name)) {
            $this->sanitizeName($data->name, $address, $errors);
        } else {
            $errors['name']="Le champ nom est obligatoire";
        }

        if (isset($data->city)) {
            $this->sanitizeName($data->city, $address, $errors);
        } else {
            $errors['city']="Le champ ville est obligatoire";
        }

        if (isset($data->streetAddress1)) {
            $this->sanitizeStreetAddress1($data->streetAddress1, $address, $errors);
        } else {
            $errors['streetAddress']="Le champ voie est obligatoire";
        }

        if (isset($data->streetAddress2)) {
            $this->sanitizeStreetAddress2($data->streetAddress2, $address, $errors);
        }

        if (isset($data->postalCode)) {
            $this->sanitizePostalCode($data->postalCode, $address, $errors);
        } else {
            $errors['postalCode']="Le champ code postal est obligatoire";
        }

        if (isset($data->phoneNumber)) {
            $this->sanitizePhoneNumber($data->phoneNumber, $address);
        }

    }
}