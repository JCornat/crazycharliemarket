<?php

namespace src\Controller;

use src\AbstractController;
use src\View\Home;

class IndexController extends AbstractController {

    public function index() {
        $view = new Home();
        $view->addVar('title', 'Accueil');
        $view->addVar('session', $_SESSION);
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }
}