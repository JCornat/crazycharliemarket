<?php


namespace src\Controller;


use src\AbstractController;
use src\Entity\Type;
use src\Entity\User;

class UserController extends AbstractController {

    public function login() {
        $data = array();
        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        if(!isset($param->email) ||!isset($param->password)) {
            $data['status'] = 400;
            echo json_encode($data);
            return;
        }

        $user = $this->em->getRepository('src\Entity\User')->findOneBy(array('email' => $param->email));

        if($user == null) {
            $data['status'] = 400;
        } else {
            if(password_verify($param->password, $user->getPassword())) {
                $_SESSION['id'] = $user->getId();

                if($user->isAdmin()) {
                    $_SESSION['admin'] = true;
                }
            } else {
                $data['status'] = 400;
            }
        }

        echo json_encode($data);
        return;
    }


    public function register() {
        $data = array();
        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $errors = array();
            $user = new User();

            $list = $this->em->getRepository('src\Entity\User')->findBy(array('email' => $param->email));
            if(sizeof($list) > 0) {
                throw new \Exception('Email déjà utilisé');
            }

            $user->setEmail($param->email);
            $user->setPassword(password_hash($param->password, PASSWORD_DEFAULT, array('cost'=> 12)));

            $this->em->persist($user);
            $this->em->flush();
            $_SESSION['uid'] = $user->getId();
            $data['uid'] = $user->getId();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }


}