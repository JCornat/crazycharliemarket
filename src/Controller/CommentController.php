<?php


namespace src\Controller;


use src\AbstractController;
use src\Entity\Comment;
use src\Entity\Item;

class CommentController extends AbstractController {

    //TODO Check rights
    public function getFromItem($id) {
        $data = array();
        $data['status'] = 200;
        $body = $this->app->request->getBody();

        error_log("AA");
        try {
            $item = $this->em->find('src\Entity\Item', $id);
            if($item == null) {
                throw new \Exception('Item not found');
            }
            error_log("CC");
            error_log(sizeof($item->getComments()));

            foreach ($item->getComments() as $comment) {
                error_log("BB");
                $data['comments'][] = $this->commentToArray($comment);
            }

        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function commentInItem($id) {
        $data = array();
        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $item = $this->em->getRepository('src\Entity\Item')->find($id);
            if($item == null) {
                throw new \Exception('Item not found');
            }

            $comment = new Comment();
            $comment->setItem($item);
            $comment->setName($param->name);
            $comment->setComment($param->comment);
            $comment->setGrade($param->grade);

            $this->em->persist($comment);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }


    private function commentToArray(Comment $comment) {
        return array(
            'id' => $comment->getId(),
            'name' => $comment->getName(),
            'comment' => $comment->getComment(),
            'grade' => $comment->getGrade(),
            'createdAt' => $comment->getCreatedAt()->format('d-m H:i'),
            'updatedAt' => $comment->getUpdatedAt()->format('d-m H:i'),
        );
    }

}