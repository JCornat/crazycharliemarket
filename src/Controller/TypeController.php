<?php


namespace src\Controller;


use src\AbstractController;
use src\Entity\Type;

class TypeController extends AbstractController {

    //TODO Check rights
    public function get($id = null) {
        $data = array();
        $data['status'] = 200;

        try {
            if ($id === null) {
                $types = $this->em->getRepository('src\Entity\Type')->findAll();
                foreach ($types as $type) {
                    $data['types'][] = $this->typeToArray($type);
                }
            } else {
                $type = $this->em->getRepository('src\Entity\Type')->find(array('id' => $id));
                if($type == null) {
                    throw new \Exception('Type not found');
                }
                $data['types'] = $this->typeToArray($type);
            }
        } catch (\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function add() {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $type = new Type();
            $type->setName($param->name);
            $type->setDescription($param->description);
            $type->setImage($param->image);

            $this->em->persist($type);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function edit($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();
        $param = json_decode($body);

        try {
            $type = $this->em->find('src\Entity\Type', $id);
            if($type == null) {
                throw new \Exception('Type not found');
            }

            $type->setName($param->name);
            $type->setDescription($param->description);
            $type->setImage($param->image);

            $this->em->persist($type);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    public function delete($id) {
        $data = array();
        if(!array_key_exists('admin', $_SESSION)) {
            $data['message'] = "NICE TRY";
            $data['status'] = 400;
            return;
        }

        $data['status'] = 200;
        $body = $this->app->request->getBody();

        try {
            $type = $this->em->find('src\Entity\Type', $id);
            if($type == null) {
                throw new \Exception('Type not found');
            }

            $this->em->remove($type);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['status'] = 400;
            $data['message'] = $e->getMessage();
        }

        echo json_encode($data);
        return;
    }

    private function typeToArray(Type $type) {
        return array(
            'id' => $type->getId(),
            'name' => $type->getName(),
            'description' => $type->getDescription(),
            'image' => $type->getImage(),
            'createdAt' => $type->getCreatedAt()->format('d-m H:i'),
            'udpatedAt' => $type->getCreatedAt()->format('d-m H:i'),
        );
    }

}