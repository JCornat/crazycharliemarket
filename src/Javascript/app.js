var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {templateUrl:'src/Template/home.html', controller:'HomeCtrl'})
        .when('/login', {templateUrl:'src/Template/User/login.html', controller:'LoginCtrl'})
        .when('/register', {templateUrl:'src/Template/User/register.html', controller:'RegisterCtrl'})
        .when('/types', {templateUrl:'src/Template/Type/typeList.html', controller:'TypeListCtrl'})
        .when('/types/add', {templateUrl:'src/Template/Type/edit.html', controller:'TypeAddCtrl'})
        .when('/types/:id/edit', {templateUrl:'src/Template/Type/edit.html', controller:'TypeEditCtrl'})
        .when('/cart', {templateUrl:'src/Template/Cart/status.html', controller:'CartCtrl'})
        .when('/buy/info', {templateUrl:'src/Template/Cart/info.html', controller:'InfoCtrl'})
        .when('/item', {templateUrl:'src/Template/Item/list.html', controller:'ItemCtrl'})
        .when('/item/add', {templateUrl:'src/Template/Item/edit.html', controller:'ItemAddCtrl'})
        .when('/item/:id', {templateUrl:'src/Template/Item/detail.html', controller:'ItemDetailCtrl'})
        .when('/item/:id/edit', {templateUrl:'src/Template/Item/edit.html', controller:'ItemEditCtrl'})
        .when('/color', {templateUrl:'src/Template/Color/list.html', controller:'ColorCtrl'})
        .when('/color/add', {templateUrl:'src/Template/Color/edit.html', controller:'ColorAddCtrl'})
        .when('/color/:id/edit', {templateUrl:'src/Template/Color/edit.html', controller:'ColorEditCtrl'})
        .when('/piece', {templateUrl:'src/Template/Piece/list.html', controller:'PieceCtrl'})
        .when('/piece/add', {templateUrl:'src/Template/Piece/edit.html', controller:'PieceAddCtrl'})
        .when('/piece/:id/edit', {templateUrl:'src/Template/Piece/edit.html', controller:'PieceEditCtrl'})
        .when('/type', {templateUrl:'src/Template/Type/list.html', controller:'TypeCtrl'})
        .when('/type/:id', {templateUrl:'src/Template/Type/list.html', controller:'TypeCtrl'})
        .when('/type/:id/edit', {templateUrl:'src/Template/Type/edit.html', controller:'TypeEditCtrl'})
        .otherwise({redirectTo: '/'});

    $locationProvider.html5Mode(true);

}]);

app.filter('range', function() {
    return function(input) {
        var lowBound, highBound;
        switch (input.length) {
            case 1:
                lowBound = 0;
                highBound = parseInt(input[0]) - 1;
                break;
            case 2:
                lowBound = parseInt(input[0]);
                highBound = parseInt(input[1]);
                break;
            default:
                return input;
        }
        var result = [];
        for (var i = lowBound; i <= highBound; i++)
            result.push(i);
        return result;
    };
});


app.run(['$rootScope', '$route', '$location', 'loginService', '$routeParams', function($rootScope, $route, $location, loginService, $routeParams) {

    var adminPermission = [
        {regexp:'^/types$'},
        {regexp:'^/item/add'},
        {regexp:'^/item$'},
        {regexp:'^/item/*/edit'},
        {regexp:'^/color*'},
        {regexp:'^/piece*'},
        {regexp:'^/type/add$'},
        {regexp:'^/type/*/edit$'}
    ];

    $rootScope.$on('$routeChangeStart', function () {
        var checkConnexion = null;

        for (var i = 0; i < adminPermission.length; i++) {
            var cond = false;
            if(adminPermission[i].regexp != undefined) {
                var regexp = new RegExp(adminPermission[i].regexp, "i");
                cond = (regexp.test($location.path()) === true);
            } else if(adminPermission[i].route != undefined) {
                cond = (userPermission[i].route === $location.path());
            }
            if(cond) {
                checkConnexion = loginService.isAdmin();
                checkConnexion.then(function(response) {
                    var data = response.data;
                    if(data.status != 200) {
                        $location.path('/login');
                    }
                    console.log("Admin rights checked");
                }, function () {
                    alert('ERREUR 3074');
                    $location.path('/home');
                });
            }
        }

    });
}]);
