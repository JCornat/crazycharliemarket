app.controller('HomeCtrl', ['$scope', '$location', '$http', 'cartService', function ($scope, $location, $http, cartService) {

    $scope.login = function () {
        $location.path('/login');
    };

    $scope.cart = rootpath+'/cart';

    $scope.register = function () {
        $location.path('/register')
    };

    $scope.typeList = function (id) {
        $location.path('/type/'+id);
    };

    $http.get(rootpath+'/api/items')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.items = data.items;
                $scope.selectedItems = [];
                for (var i = 0; i < $scope.items.length; i++) {
                    console.log($scope.items[i]);
                    if($scope.items[i].selectedItem === true) {
                        $scope.selectedItems.push($scope.items[i]);
                    }
                }
                console.log($scope.selectedItems);
            }
        });

    $http.get(rootpath+'/api/types')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.types = data.types;
            }
        });

    $scope.detail = function (id) {
        $location.path('/item/'+id);
    };

    $scope.addToCart = function (id, $event) {
        var button = $event.currentTarget;
        button.value="Ajouté !";
        button.disabled=true;

        cartService.addToCart(id);

    };

    $scope.products = function () {
        $location.path(rootpath+'/type');
    };

}]);
