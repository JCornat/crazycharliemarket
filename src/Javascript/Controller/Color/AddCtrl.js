app.controller('ColorAddCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $scope.title = "Ajouter une couleur";

    $scope.submit = function () {
        var data = $scope.color;
        if(data.name != null && data.hex != null) {
            $http.post(rootpath+'/api/colors/add', data)
                .then(function(d){
                    var result = d.data;
                    if(result.status == 200) {
                        $location.path('/color');
                    } else {
                        alert('Erreur');
                        console.log(d);
                    }
                }, function(error) {
                    console.log(error);
                });
        }
    };

}]);
