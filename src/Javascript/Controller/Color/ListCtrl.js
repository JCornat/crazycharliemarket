app.controller('ColorCtrl', ['$scope', '$location', '$http', function ($scope, $location, $http) {

    $http.get(rootpath+'/api/colors')
        .then(function (results) {
            console.log(results);
            var data = results.data;
            if(data.status == 200) {
                $scope.colors = data.colors;
            }
        });

    $scope.addColor = rootpath+'/color/add';

    $scope.colorEdit = function (id) {
        console.log(id);
        $location.path('/color/'+id+'/edit');
    };

    $scope.colorDelete = function (id) {
        $http.delete(rootpath+'/api/colors/'+id+'/delete')
            .then(function (res) {
                var data = res.data;
                if(data.status == 200) {
                    for (var i = 0; i < $scope.colors.length; i++) {
                        if($scope.colors[i].id == id) {
                            $scope.colors.splice(i,1);
                        }
                    }
                }
            }, function () {

            });
    };

}]);
