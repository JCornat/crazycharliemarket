app.controller('ColorEditCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $http.get(rootpath+'/api/colors/'+$routeParams.id)
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.color = data.colors;
            } else {
                $location.path(rootpath+'/color');
            }
        });

    $scope.submit = function () {
        var data = $scope.color;
        if(data.name != null && data.hex != null) {
            $http.post(rootpath+'/api/colors/'+$routeParams.id+'/edit', data)
                .then(function(d){
                    var result = d.data;
                    if(result.status == 200) {
                        $location.path('/color');
                    } else {
                        alert('Erreur');
                        console.log(d);
                    }
                }, function(error) {
                    console.log(error);
                });
        }
    };

}]);
