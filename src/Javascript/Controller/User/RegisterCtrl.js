app.controller('RegisterCtrl', ['$scope', '$location', 'loginService', function ($scope, $location, loginService) {

    $scope.login = function () {
        $location.path('/login');
    };

    $scope.register = function () {
        loginService.register($scope.user, $scope);
    };
}]);