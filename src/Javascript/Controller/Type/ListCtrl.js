app.controller('TypeCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $scope.home = rootpath+"/";

    $scope.cart = rootpath+'/cart';

    $http.get(rootpath+'/api/items')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.items = data.items;
            }
        });

    $http.get(rootpath+'/api/types')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                for (var i = 0; i < data.types.length; i++) {
                    if($routeParams.id == data.types[i].id) {
                        $scope.id = data.types[i].id;
                        $scope.search = {
                            typeId : data.types[i].id
                        }
                    }
                }
                $scope.types = data.types;
            }
        });

    $http.get(rootpath+'/api/pieces')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.pieces = data.pieces;
            }
        });

    $http.get(rootpath+'/api/colors')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.colors = data.colors;
            }
        });

    $scope.typeList = function (id) {
        $location.path('/type/'+id);
    };


    $scope.detail = function (id) {
        $location.path('/item/'+id);
    };

    $scope.buy = function (id) {
        console.log("Add in cart item "+id);
        if(localStorage.getItem(id) === null) {
            localStorage.setItem(id, 1);
        } else {
            localStorage.setItem(id, (localStorage.getItem(id) * 1 + 1 * 1));
        }

    };
}]);
