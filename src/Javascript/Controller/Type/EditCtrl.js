app.controller('TypeEditCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $scope.title = "Editer un type";

    $http.get(rootpath+'/api/types/'+$routeParams.id)
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.type = data.types;
            } else {
                $location.path(rootpath+'/types');
            }
        });

    $scope.submit = function () {
        var data = $scope.type;
        if(data.name != null && data.hex != null) {
            $http.post(rootpath+'/api/types/'+$routeParams.id+'/edit', data)
                .then(function(d){
                    var result = d.data;
                    if(result.status == 200) {
                        $location.path('/types');
                    } else {
                        alert('Erreur');
                        console.log(d);
                    }
                }, function(error) {
                    console.log(error);
                });
        }
    };

    $scope.submit = function () {
        var data = $scope.type;
        $http.post(rootpath+'/api/types/'+$routeParams.id+'/edit', data)
            .then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/types');
                } else {
                    alert('Erreur');
                    console.log(d);
                }
            }, function(error) {
                console.log(error);
            });
    };

}]);
