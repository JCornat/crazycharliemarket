app.controller('TypeListCtrl', ['$scope', '$location', '$http', function ($scope, $location, $http) {

    $http.get(rootpath+'/api/types')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.types = data.types;
            }
        });

    $scope.addColor = rootpath+'/types/add';

    $scope.typeEdit = function (id) {
        $location.path('/types/'+id+'/edit');
    };

    $scope.typeDelete = function (id) {
        $http.delete(rootpath+'/api/types/'+id+'/delete')
            .then(function (res) {
                var data = res.data;
                if(data.status == 200) {
                    for (var i = 0; i < $scope.types.length; i++) {
                        if($scope.types[i].id == id) {
                            $scope.types.splice(i,1);
                        }
                    }
                }
            }, function () {

            });
    };

}]);
