app.controller('TypeAddCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $scope.title = "Ajouter un type";

    $scope.submit = function () {
        var data = $scope.type;
        $http.post(rootpath+'/api/types/add', data)
            .then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/types');
                } else {
                    alert('Erreur');
                    console.log(d);
                }
            }, function(error) {
                console.log(error);
            });
    };

}]);
