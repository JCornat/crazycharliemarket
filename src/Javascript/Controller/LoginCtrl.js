app.controller('LoginCtrl', ['$scope', '$location', 'loginService', function ($scope, $location, loginService) {
    $scope.login = function () {
        loginService.login($scope.user, $scope);
    };

    $scope.register = function () {
        $location.path('/register')
    };
}]);