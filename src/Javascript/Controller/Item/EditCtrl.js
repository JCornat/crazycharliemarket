app.controller('ItemEditCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $scope.title = "Editer un item";

    $scope.submit = function () {
        var data = $scope.item;
        $http.post(rootpath+'/api/items/'+$routeParams.id+'/edit', data)
            .then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/item/'+$routeParams.id);
                } else {
                    alert('Erreur');
                    console.log(d);
                }
            }, function(error) {
                console.log(error);
            });
    };

    $http.get(rootpath+'/api/items/'+$routeParams.id)
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.item = data.items;
                $http.get(rootpath+'/api/types')
                    .then(function (results) {
                        var data = results.data;
                        if(data.status == 200) {
                            $scope.types = data.types;
                        }
                    });
            } else {
                $location.path(rootpath+'/');
            }
        });


    $http.get(rootpath+'/api/colors')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.colors = data.colors;
            }
        });

    $http.get(rootpath+'/api/pieces')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.pieces = data.pieces;
            }
        });

}]);
