app.controller('ItemAddCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $scope.title = "Ajouter un item";

    $scope.item = {
        popularity : 0
    };

    $scope.submit = function () {
        var data = $scope.item;
        $http.post(rootpath+'/api/items/add', data)
            .then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/type');
                } else {
                    alert('Erreur');
                    console.log(d);
                }
            }, function(error) {
                console.log(error);
            });
    };

    $http.get(rootpath+'/api/types')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.types = data.types;
            }
        });

    $http.get(rootpath+'/api/colors')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.colors = data.colors;
            }
        });

    $http.get(rootpath+'/api/pieces')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.pieces = data.pieces;
            }
        });

}]);
