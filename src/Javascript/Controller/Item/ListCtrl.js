app.controller('ItemCtrl', ['$scope', '$location', '$http', function ($scope, $location, $http) {

    $http.get(rootpath+'/api/items')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.items = data.items;
            }
        });

    $scope.addItem = rootpath+'/item/add';

    $scope.itemEdit = function (id) {
        console.log(id);
        $location.path('/item/'+id+'/edit');
    };

    $scope.itemDelete = function (id) {
        $http.delete(rootpath+'/api/items/'+id+'/delete')
            .then(function (res) {
                var data = res.data;
                if(data.status == 200) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        if($scope.items[i].id == id) {
                            $scope.items.splice(i,1);
                        }
                    }
                }
            }, function () {

            });
    };

    $scope.selectedItem = function (id) {
        for (var i = 0; i < $scope.items.length; i++) {
            if($scope.items[i].id === id) {
                $http.post(rootpath+'/api/items/'+id+'/checked', true)
                    .then(function (res) {
                        console.log(res);
                    }, function () {

                    });
                break;
            }
        }
    };

}]);
