app.controller('ItemDetailCtrl', ['$scope', '$location', '$http', '$routeParams', 'cartService', function ($scope, $location, $http, $routeParams, cartService) {

    $scope.voted = false;

    $scope.home = rootpath+'/';

    $scope.cart = rootpath+'/cart';

    $scope.myComment = {
        grade: 5
    };

    calcAverage = function () {
        $scope.average = 0;
        for (var i = 0; i < $scope.comments.length; i++) {
            $scope.average = ($scope.average * 1 + $scope.comments[i].grade * 1);
        }
        $scope.average = $scope.average/$scope.comments.length;
    };

    $http.get(rootpath+'/api/items/'+$routeParams.id)
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.item = data.items;
                console.log($scope.item.image);
                $scope.product = {
                    image:$scope.item.image
                };
                console.log($scope.product);
            } else {
                $location.path(rootpath+'/')
            }
        });

    $http.get(rootpath+'/api/items/'+$routeParams.id+'/comments')
        .then(function (results) {
            var data = results.data;
            console.log(data);
            if(data.status == 200) {
                $scope.comments = data.comments;
                calcAverage();
            }
        });

    $scope.plusOne = function () {
        $scope.voted = true;
        $http.post(rootpath+'/api/items/'+$routeParams.id+'/plus')
            .then(function (res) {
                var data = res.data;
                if(data.status == 200) {
                    $scope.item.popularity++;
                }
            });
    };

    $scope.postComment = function () {
        $http.post(rootpath+'/api/items/'+$routeParams.id+'/comments', $scope.myComment)
            .then(function (res) {
                var data = res.data;
                if(data.status == 200) {
                    if(typeof($scope.comments) == "undefined" || $scope.comments.length == 0) {
                        $scope.comments = [];
                    }
                    $scope.commentPosted = true;
                    $scope.comments.push($scope.myComment);
                    calcAverage();
                } else {
                    alert("Error");
                }
            });
    };


    $scope.addToCart = function ($event) {
        var button = $event.currentTarget;
        button.value="Ajouté !";
        button.disabled=true;

        cartService.addToCart($routeParams.id);
    };

}]);
