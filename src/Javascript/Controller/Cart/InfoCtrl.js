app.controller('InfoCtrl', ['$scope', '$location', '$http', 'cartService', function ($scope, $location, $http, cartService) {

    $scope.items = [];


    sum = function () {
        $scope.totalPrice = 0;
        for (var i = 0; i < $scope.items.length; i++) {
            $scope.totalPrice += ($scope.items[i].quantity * $scope.items[i].price)
        }
    };

    for (var i = 0; i < localStorage.length; i++) {
        $http.get(rootpath+'/api/items/'+localStorage.key(i))
            .then(function (results) {
                var data = results.data;
                if(data.status == 200) {
                    data.items.quantity = localStorage.getItem(data.items.id);
                    $scope.items.push(data.items);
                    sum();
                } else {
                    console.log(data);
                }
            });
    }

    $scope.minus = function (id) {
        for (var i = 0; i < $scope.items.length; i++) {
            if($scope.items[i].id == id) {
                $scope.items[i].quantity--;
                if($scope.items[i].quantity < 0) {
                    $scope.items[i].quantity = 0;
                }
                localStorage.setItem(id, $scope.items[i].quantity);
                sum();
                break;
            }
        }
    };

    $scope.plus = function (id) {
        for (var i = 0; i < $scope.items.length; i++) {
            if($scope.items[i].id == id) {
                $scope.items[i].quantity++;
                localStorage.setItem(id, $scope.items[i].quantity);
                sum();
                break;
            }
        }
    };

    $scope.deleteProduct = function (id) {
        console.log("request" + id);
        for (var i = 0; i < $scope.items.length; i++) {
            if($scope.items[i].id == id) {
                localStorage.removeItem(id);
                $scope.items.splice(i,1);
                sum();
                break;
            }
        }
    };

    $scope.buy = function () {
        localStorage.clear();
        $location.path(rootpath+'/');
    }


}]);
