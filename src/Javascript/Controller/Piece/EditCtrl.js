app.controller('PieceEditCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $http.get(rootpath+'/api/pieces/'+$routeParams.id)
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.piece = data.pieces;
            } else {
                $location.path(rootpath+'/piece');
            }
        });

    $scope.submit = function () {
        var data = $scope.piece;
        $http.post(rootpath+'/api/pieces/'+$routeParams.id+'/edit', data)
            .then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/piece');
                } else {
                    alert('Erreur');
                    console.log(d);
                }
            }, function(error) {
                console.log(error);
            });
    };

}]);
