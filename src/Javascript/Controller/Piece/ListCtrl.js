app.controller('PieceCtrl', ['$scope', '$location', '$http', function ($scope, $location, $http) {

    $http.get(rootpath+'/api/pieces')
        .then(function (results) {
            var data = results.data;
            if(data.status == 200) {
                $scope.pieces = data.pieces;
            }
        });

    $scope.addPiece = rootpath+'/piece/add';

    $scope.pieceEdit = function (id) {
        $location.path(rootpath+'/piece/'+id+'/edit');
    };

    $scope.pieceDelete = function (id) {
        $http.delete(rootpath+'/api/pieces/'+id+'/delete')
            .then(function (res) {
                var data = res.data;
                if(data.status == 200) {
                    for (var i = 0; i < $scope.pieces.length; i++) {
                        if($scope.pieces[i].id == id) {
                            $scope.pieces.splice(i,1);
                        }
                    }
                }
            }, function () {

            });
    };

}]);
