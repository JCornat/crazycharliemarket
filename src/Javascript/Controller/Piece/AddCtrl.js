app.controller('PieceAddCtrl', ['$scope', '$location', '$http', '$routeParams', function ($scope, $location, $http, $routeParams) {

    $scope.title = "Ajouter une pièce";

    $scope.submit = function () {
        var data = $scope.piece;
        $http.post(rootpath+'/api/pieces/add', data)
            .then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/piece');
                } else {
                    alert('Erreur');
                    console.log(d);
                }
            }, function(error) {
                console.log(error);
            });
    };

}]);
