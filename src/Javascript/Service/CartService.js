app.factory('cartService', ['$http', '$location', '$rootScope', function($http, $location, $rootScope) {
    return {
        addToCart: function (id) {
            if(localStorage.getItem(id) === null) {
                localStorage.setItem(id, 1);
            } else {
                localStorage.setItem(id, (localStorage.getItem(id) * 1 + 1 * 1));
            }
        }
    }
}]);
