app.factory('loginService', ['$http', '$location', function($http, $location) {
    return {
        login: function(data, scope) {
            var $promise = $http.post(rootpath+'/api/users/login', data);
            $promise.then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/purchase');
                } else {
                    scope.error = "Mauvais mot de passe ou adresse email";
                }
            }, function(error) {
                console.log(error);
            });
        },
        register: function(data, scope) {
            $http.post(rootpath+'/api/users/register', data)
                .then(function(d){
                    var result = d.data;
                    if(result.status == 200) {
                        $location.path('/account');
                    } else {
                        scope.error = result.message;
                    }
                }, function(error) {
                    console.log(error);
                });
        },
        logout: function() {
            $http.get(rootpath+'/api/session/destroy')
                .then(function() {
                    $location.path('/home');
                }
            );
        },
        isLogged: function() {
            return $http.get(rootpath+'/api/session/check');
        },
        getProfile: function() {
            return $http.get(rootpath+'/api/session/profile');
        },
        setProfile: function(data) {
            return $http.post(rootpath+'/api/session/profile', data);
        },
        isAdmin: function() {
            return $http.get(rootpath+'/api/admin/check');
        },
        getAddresses: function() {
            return $http.get(rootpath+'/api/addresses/');
        },
        getAddress: function(id) {
            return $http.get(rootpath+'/api/addresses/'+id);
        },
        getConnexions: function() {
            return $http.get(rootpath+'/api/connexions/');
        },
        addAddress: function(data) {
            return $http.post(rootpath+'/api/addresses/add', data);
        },
        editAddress: function(data) {
            return $http.post(rootpath+'/api/addresses/'+data.id+'/edit', data);
        },
        addOrder: function(data, scope) {
            var $promise = $http.post(rootpath+'/api/orders/add', data);
            $promise.then(function(d){
                var result = d.data;
                if(result.status == 200) {
                    $location.path('/order');
                } else {
                    scope.error = result.message;
                }
            }, function(error) {
                console.log(error);
            });
        }
    }
}]);
