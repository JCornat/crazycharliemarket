<?php

namespace src;


abstract class AbstractView {
    protected $layout;
    protected $layoutAjax;
    protected $data = [];
    protected $obj;

    public function __construct($o = null) {
        $this->obj = $o;
    }

    public function addVar($key, $value) {
        $this->data[$key] = $value;
    }

    public function render() {
        $loader = new \Twig_Loader_Filesystem('src/Template');
        $twig = new \Twig_Environment($loader, array());
        $app = \Slim\Slim::getInstance();
        $env = $app->environment();
        $twig->addGlobal('rootPath', $env['SCRIPT_NAME']);
        $template = $twig->loadTemplate($this->layout);
        return $template->render($this->data);
    }
}