<?php
session_start();

require "bootstrap.php";

$app = new \Slim\Slim();

$env = $app->environment();
if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', $env['SCRIPT_NAME']);
}

/*-----------------------*\
         Index
\*-----------------------*/

$app->get('/', function() use ($app) {
    $c = new src\Controller\IndexController($app->request);
    $c->index();
})->name('index');

$app->group('/api', 'APIMiddleWare', function() use ($app, $entityManager) {


    $app->group('/items', function() use ($app, $entityManager) {

        $app->get('(/)', function() use ($app) {
            $controller = new src\Controller\ItemController($app->request);
            $controller->get();
        });

        $app->post('/add(/)', function() {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ItemController($app->request);
            $controller->add();
        });

        $app->post('/:id/plus(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ItemController($app->request);
            $controller->plus($id);
        });

        $app->get('/:id/comments(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\CommentController($app->request);
            $controller->getFromItem($id);
        });

        $app->post('/:id/comments(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\CommentController($app->request);
            $controller->commentInItem($id);
        });

        $app->get('/:id(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ItemController($app->request);
            $controller->get($id);
        });

        $app->post('/:id/edit(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ItemController($app->request);
            $controller->edit($id);
        });

        $app->post('/:id/checked(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ItemController($app->request);
            $controller->checked($id);
        });

        $app->delete('/:id/delete(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ItemController($app->request);
            $controller->delete($id);
        });

    });

    $app->group('/types', function() use ($app, $entityManager) {

        $app->get('(/)', function() use ($app) {
            $controller = new src\Controller\TypeController($app->request);
            $controller->get();
        });

        $app->post('/add(/)', function() {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\TypeController($app->request);
            $controller->add();
        });

        $app->get('/:id(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\TypeController($app->request);
            $controller->get($id);
        });

        $app->post('/:id/edit(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\TypeController($app->request);
            $controller->edit($id);
        });

        $app->delete('/:id/delete(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\TypeController($app->request);
            $controller->delete($id);
        });

    });

    $app->group('/colors', function() use ($app, $entityManager) {

        $app->get('(/)', function() use ($app) {
            $controller = new src\Controller\ColorController($app->request);
            $controller->get();
        });

        $app->post('/add(/)', function() {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ColorController($app->request);
            $controller->add();
        });

        $app->get('/:id(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ColorController($app->request);
            $controller->get($id);
        });

        $app->post('/:id/edit(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ColorController($app->request);
            $controller->edit($id);
        });

        $app->delete('/:id/delete(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\ColorController($app->request);
            $controller->delete($id);
        });

    });


    $app->group('/users', function() use ($app, $entityManager) {

        $app->post('/login(/)', function() {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\UserController($app->request);
            $controller->login();
        });

        $app->post('/register(/)', function() {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\UserController($app->request);
            $controller->register();
        });

    });

    $app->get('/admin/check', function() {
        $app = \Slim\Slim::getInstance();
        $authenticationController = new \src\Controller\AuthenticationController($app->request);
        $authenticationController->checkAdminRights();
    });

    $app->group('/pieces', function() use ($app, $entityManager) {

        $app->get('(/)', function() use ($app) {
            $pieceController = new src\Controller\PieceController($app->request);
            $pieceController->get();
        });

        $app->post('/add(/)', function() {
            $app = \Slim\Slim::getInstance();
            $pieceController = new \src\Controller\PieceController($app->request);
            $pieceController->add();
        });

        $app->get('/:id(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $pieceController = new \src\Controller\PieceController($app->request);
            $pieceController->get($id);
        });

        $app->post('/:id/edit(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $pieceController = new \src\Controller\PieceController($app->request);
            $pieceController->edit($id);
        });

        $app->delete('/:id/delete(/)', function($id) {
            $app = \Slim\Slim::getInstance();
            $controller = new \src\Controller\PieceController($app->request);
            $controller->delete($id);
        });

    });

    $app->group('/session', function() use ($app, $entityManager) {

        $app->get('/check', function () {
            $app = \Slim\Slim::getInstance();
            $authenticationController = new \src\Controller\AuthenticationController($app->request);
            $authenticationController->checkSession();
        });

        $app->get('/logout', function () {
            $app = \Slim\Slim::getInstance();
            $authenticationController = new \src\Controller\AuthenticationController($app->request);
            $authenticationController->destroySession();
        });

    });

});


/*-----------------------*\
         Matches all routes
\*-----------------------*/

$app->get('/:method', function() use ($app) {
    $c = new src\Controller\IndexController($app->request);
    $c->index();
})->conditions(array('method' => '.+'));

function generateToken() {
    $tokenBin = openssl_random_pseudo_bytes(20);
    return bin2hex($tokenBin);
}

function APIMiddleWare() {
    $app = \Slim\Slim::getInstance();
    $response = $app->response();
    $response->header('Access-Control-Allow-Origin', '*');
    $response->header('Content-Type', 'application/json');
}

function checkAdmin() {
    $app = \Slim\Slim::getInstance();
    if(!array_key_exists('admin', $_SESSION)) {
        $app->flash('error', 'Vous n\'avez pas les droits nécessaires');
        $app->redirect($app->urlFor('admin'));
    }
}

$app->run();