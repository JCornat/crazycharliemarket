CREATE DATABASE  IF NOT EXISTS `crazyCharlieMarket` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `crazyCharlieMarket`;
-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: crazyCharlieMarket
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hex` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `items_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C2BEC39F6BB0AE84` (`items_id`),
  CONSTRAINT `FK_C2BEC39F6BB0AE84` FOREIGN KEY (`items_id`) REFERENCES `items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'Red','FF0FF0','0000-00-00 00:00:00','2015-03-05 14:29:46',NULL,NULL),(2,'Vert','00FF00','2015-03-05 13:24:47','2015-03-05 13:50:00',NULL,NULL);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `grade` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5F9E962A126F525E` (`item_id`),
  CONSTRAINT `FK_5F9E962A126F525E` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (5,1,'zqdqzdq','dzqdqzdq',2,'2015-03-05 15:31:28','2015-03-05 15:31:28',NULL),(6,1,'AAAAAAAAAA','zqdzqdqzdzqd',5,'2015-03-05 15:33:51','2015-03-05 15:33:51',NULL),(7,1,'zqdqzd','qzdzqd',1,'2015-03-05 15:36:27','2015-03-05 15:36:27',NULL),(8,2,'zqdqzd','zdqqd',1,'2015-03-05 15:39:41','2015-03-05 15:39:41',NULL),(9,2,'zqdzqd','zqdqzd',2,'2015-03-05 15:40:00','2015-03-05 15:40:00',NULL),(10,1,'dzqdzqdq','qdzq',1,'2015-03-05 15:46:56','2015-03-05 15:46:56',NULL),(11,1,'qzdqz','zqd',5,'2015-03-05 15:48:54','2015-03-05 15:48:54',NULL),(12,1,'OEOEOEO','zdqldlqzdlj',3,'2015-03-05 16:02:07','2015-03-05 16:02:07',NULL),(13,1,'azdqd','qzdq',5,'2015-03-05 16:07:36','2015-03-05 16:07:36',NULL),(14,2,'qzdqzdqzd','zqdqzdzqdz',5,'2015-03-05 16:36:22','2015-03-05 16:36:22',NULL),(15,1,'zqdqdzqd','zqdzqdzq',5,'2015-03-05 17:02:48','2015-03-05 17:02:48',NULL),(16,1,'dzqdqzdqz','zqdzqdzqdzqdqzd',2,'2015-03-05 17:25:01','2015-03-05 17:25:01',NULL),(17,2,'dqzdzdqzd','qzdqzdqz',5,'2015-03-05 17:49:08','2015-03-05 17:49:08',NULL),(18,3,'Bof','Je suis pas content !',1,'2015-03-05 18:03:06','2015-03-05 18:03:06',NULL),(19,1,'zqdqzdqz','zqdqzd',5,'2015-03-05 18:20:45','2015-03-05 18:20:45',NULL),(20,2,'qdzqdzd','zqdqzdzq',4,'2015-03-05 18:37:24','2015-03-05 18:37:24',NULL),(21,3,'zdzqzqd','dzqdqz',5,'2015-03-05 20:34:29','2015-03-05 20:34:29',NULL),(22,4,'zqdqzd','zqdqzdzq',5,'2015-03-05 20:51:33','2015-03-05 20:51:33',NULL),(23,4,'dzqzd','zqdqzd',1,'2015-03-05 20:51:43','2015-03-05 20:51:43',NULL),(24,2,'qzdqzdqz','dzqdzqdqz',5,'2015-03-05 21:34:44','2015-03-05 21:34:44',NULL),(25,5,'zqdqzdqd','qzdqdqzd',5,'2015-03-05 21:36:15','2015-03-05 21:36:15',NULL),(26,5,'zqdqzdqd','qzdqdqzd',5,'2015-03-05 21:36:18','2015-03-05 21:36:18',NULL),(27,5,'zqdqzdqd','qzdqdqzd',5,'2015-03-05 21:38:20','2015-03-05 21:38:20',NULL),(28,5,'dqzdqzd','zqdqzd',5,'2015-03-05 21:38:28','2015-03-05 21:38:28',NULL),(29,6,'dzqdqzd','zqdzqd',1,'2015-03-05 21:38:48','2015-03-05 21:38:48',NULL),(30,7,'qzdqdq','dzqdqzd',1,'2015-03-05 21:39:16','2015-03-05 21:39:16',NULL),(32,3,'zqdqzdzqdz','zqdqzzqdzqd',5,'2015-03-05 22:08:10','2015-03-05 22:08:10',NULL),(33,3,'zqdzqdzqd','zqdzqd',5,'2015-03-05 22:08:19','2015-03-05 22:08:19',NULL),(34,7,'qdzqzd','qzdqzd',5,'2015-03-05 22:15:42','2015-03-05 22:15:42',NULL);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `piece_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `popularity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color_id` int(11) DEFAULT NULL,
  `selected` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E11EE94DC54C8C93` (`type_id`),
  KEY `IDX_E11EE94DC40FCFA8` (`piece_id`),
  KEY `IDX_E11EE94D7ADA1FB5` (`color_id`),
  CONSTRAINT `FK_E11EE94D7ADA1FB5` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`),
  CONSTRAINT `FK_E11EE94DC40FCFA8` FOREIGN KEY (`piece_id`) REFERENCES `pieces` (`id`),
  CONSTRAINT `FK_E11EE94DC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,4,4,'Fauteuil Gonflable','zqdzqd',160.99,18,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'http://service.rapide.voila.net/image/P_M_meuble-TV-teck.jpg',NULL,1),(2,1,1,'Item 2','qzdqzd',110.00,8,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'http://www.maisonsdumonde.com/images/produits/FR/fr/taille_hd/16/15/110346_1.jpg',1,0),(3,3,3,'Item 3','',130.00,4,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'http://www.huissiers-encheres.fr/site/wp-content/uploads/2013/07/meuble-tele-dangle-de-style-savoyard-en-pin-massif-106x49-mtt-wdettzielh.jpg',NULL,0),(4,1,1,'dqzd','zqdqzdzqd',150.10,4,'2015-03-05 16:23:17','2015-03-05 16:23:17',NULL,'zqdqzd',NULL,0),(5,2,1,'qzdqzd','qzdqdzqd',150.10,0,'2015-03-05 21:36:04','2015-03-05 21:36:04',NULL,'qzdqzdqzd',2,0),(6,1,2,'zdzq','qzdqzd',50.00,0,'2015-03-05 21:38:42','2015-03-05 21:38:42',NULL,'dqzdqzd',2,1),(7,2,1,'zqdq','qzdqzd',51.00,1,'2015-03-05 21:39:10','2015-03-05 21:39:10',NULL,'zdzqdqz',2,1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pieces`
--

DROP TABLE IF EXISTS `pieces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pieces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pieces`
--

LOCK TABLES `pieces` WRITE;
/*!40000 ALTER TABLE `pieces` DISABLE KEYS */;
INSERT INTO `pieces` VALUES (1,'Salon','Amusez-vous !','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(2,'Sale à manger','Régalez-vous','0000-00-00 00:00:00','2015-03-05 21:12:22',NULL),(3,'Chambre à coucher','Pour bien dommire','0000-00-00 00:00:00','2015-03-05 22:09:14',NULL),(4,'Salle de bain','On en ressort tout propre !','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `pieces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'Carton','','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'http://auto.img.v4.skyrock.net/9962/79439962/pics/photo_79439962_1.jpg'),(2,'Récupération','','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'http://static.pexels.com/wp-content/uploads/2014/06/chairs-idyllic-restaurant-852.jpg'),(3,'Gonflable','','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'https://farm4.staticflickr.com/3692/9493594797_751f98100a_b.jpg'),(4,'Recyclage','','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'https://farm6.staticflickr.com/5101/5769143413_e7dc221eab_b.jpg');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'aaa@aaa.com','$2y$12$Yv/jsDknXYg9BGNlp7CFYeQ2ath.NoLyea.2ZN4kqlukbyqCil2wa','2015-03-05 22:26:08','2015-03-05 22:26:08',NULL,1),(2,'visitor@aaa.com','$2y$12$892lI5HdMOeI/5wMm19Ys.K.vW5B7s751qzeZGv7MmYb1jNeqvnqa','2015-03-05 22:42:50','2015-03-05 22:42:51',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-05 22:48:49
